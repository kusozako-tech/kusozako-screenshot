
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale
import gi

gi.require_version('GstPlayer', '1.0')
gi.require_version('Wnck', '3.0')

VERSION = "2022.03.18"
APPLICATION_NAME = "kusozako-screenshot"
APPLICATION_ID = "com.gitlab.kusozako-tech.kusozako-screenshot"

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
    )

APPLICATION_DATA = {
    "name": APPLICATION_NAME,
    "id": APPLICATION_ID,
    "icon-name": APPLICATION_ID,
    "version": VERSION,
    "short description": _("take a screenshot"),
    "long-description": _("""screenshot application for kusozako-project""")
    }
