
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_screenshot import Size


class AlfaEntryRow(Gtk.Grid, DeltaEntity):

    LABEL = "define label text here."
    CSS = "primary-surface-color-class"

    def _on_map(self, entry):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(
            self,
            column_homogeneous=True,
            column_spacing=Size.SPACING,
            row_spacing=Size.SPACING,
            )
        label = Gtk.Label(self.LABEL, xalign=1, yalign=0.5)
        self.attach(label, 0, 0, 1, 1)
        entry = Gtk.Entry(margin=Size.SPACING)
        self.attach(entry, 1, 0, 2, 1)
        entry.connect("map", self._on_map)
        self.set_size_request(-1, Size.ROW_HEIGHT)
        self._raise("delta > css", (self, self.CSS))
        self._raise("delta > add to container", self)
