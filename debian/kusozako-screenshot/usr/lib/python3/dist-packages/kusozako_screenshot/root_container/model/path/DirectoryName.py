
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_screenshot import ModelSignals
from .Name import AlfaName


class DeltaDirectoryName(AlfaName):

    SIGNAL = ModelSignals.DIRECTORY_CHANGED

    def _action(self, name):
        gio_file = Gio.File.new_for_path(name)
        is_valid = gio_file.query_exists()
        self._raise("delta > name changed", is_valid)
