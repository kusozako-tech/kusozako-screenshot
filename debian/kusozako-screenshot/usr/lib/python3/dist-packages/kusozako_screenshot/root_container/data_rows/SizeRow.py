
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_screenshot import Size


class DeltaSizeRow(Gtk.Grid, DeltaEntity):

    LABEL = "Size :"
    CSS = "secondary-surface-color-class"

    def _on_map(self, data_label):
        pixbuf = self._enquiry("delta > pixbuf")
        text = "{} x {}".format(pixbuf.get_width(), pixbuf.get_height())
        data_label.set_text(text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(
            self,
            column_homogeneous=True,
            column_spacing=Size.SPACING,
            row_spacing=Size.SPACING,
            )
        label = Gtk.Label(self.LABEL, xalign=1, yalign=0.5)
        label.set_size_request(-1, Size.ROW_HEIGHT)
        self.attach(label, 0, 0, 1, 1)
        data_label = Gtk.Label("", xalign=0, yalign=0.5, margin=Size.SPACING)
        self.attach(data_label, 1, 0, 2, 1)
        data_label.connect("map", self._on_map)
        self.set_size_request(-1, Size.ROW_HEIGHT)
        self._raise("delta > css", (self, self.CSS))
        self._raise("delta > add to container", self)
