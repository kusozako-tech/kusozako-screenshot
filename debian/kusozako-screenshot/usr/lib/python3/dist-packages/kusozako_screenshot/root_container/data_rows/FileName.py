
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_screenshot import ModelSignals
from .EntryRow import AlfaEntryRow

FORMAT = "screenshot_%F_%T.png"


class DeltaFileName(AlfaEntryRow):

    LABEL = _("File Name :")
    CSS = "primary-surface-color-class"

    def _on_changed(self, entry):
        param = ModelSignals.FILE_NAME_CHANGED, entry.get_text()
        self._raise("delta > model signal", param)

    def _on_map(self, entry):
        entry.connect("changed", self._on_changed)
        date_time = GLib.DateTime.new_now_local()
        text = date_time.format(FORMAT)
        entry.grab_focus()
        entry.set_text(text)
        entry.select_region(0, len(text)-4)
