
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_screenshot import ModelSignals
from .FileName import DeltaFileName
from .DirectoryName import DeltaDirectoryName


class DeltaPath(DeltaEntity):

    def _check_validation(self):
        names = [self._directory_name.name, self._file_name.name]
        path = GLib.build_filenamev(names)
        gio_file = Gio.File.new_for_path(path)
        if gio_file.query_exists(None):
            self._is_valid = False
        self._path = path

    def _delta_call_name_changed(self, is_valid):
        self._is_valid = is_valid
        if is_valid:
            self._check_validation()
        param = ModelSignals.APPLYABLE_CHANGED, self._is_valid
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        self._path = ""
        self._is_valid = False
        self._file_name = DeltaFileName(self)
        self._directory_name = DeltaDirectoryName(self)

    @property
    def is_valid(self):
        return self._is_valid

    @property
    def path(self):
        return self._path
