
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .model.Model import DeltaModel
from .drawing_area.DrawingArea import DeltaDrawingArea
from .data_rows.DataRows import EchoDataRows
from .action_bar.ActionBar import DeltaActionBar


class DeltaRootContainer(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_call_register_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_model_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaModel(self)
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaDrawingArea(self)
        EchoDataRows(self)
        DeltaActionBar(self)
        self._raise("delta > add to container", self)
