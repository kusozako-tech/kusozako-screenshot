
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3 import HomeDirectory
from kusozako_screenshot import ModelSignals
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog

DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "emblem-default-symbolic",
    "buttons": (_("OK"),)
    }
MESSAGE = _("Screenshot successfully saved to \n\n{}")


class DeltaApplySignal(DeltaEntity):

    def _save_pixbuf(self):
        pixbuf = self._enquiry("delta > pixbuf")
        path = self._enquiry("delta > path")
        pixbuf.savev(path, "png", [], [])
        model = DIALOG_MODEL.copy()
        model["message"] = MESSAGE.format(HomeDirectory.shorten(path))
        _ = DeltaMessageDialog.run_for_model(self, model)
        self._raise("delta > application force quit")

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != ModelSignals.APPLY_MODEL:
            return
        if self._enquiry("delta > path is valid"):
            self._save_pixbuf()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
