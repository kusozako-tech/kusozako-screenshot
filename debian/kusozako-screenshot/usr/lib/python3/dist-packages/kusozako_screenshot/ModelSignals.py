
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

FILE_NAME_CHANGED = 0           # str as file_name
DIRECTORY_CHANGED = 1           # str as directory_name
APPLYABLE_CHANGED = 2           # bool as applyable
APPLY_MODEL = 3                 # None
