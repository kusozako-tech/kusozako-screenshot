
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import GstPlayer
from libkusozako3.Entity import DeltaEntity


class DeltaShutterSound(DeltaEntity):

    def play(self):
        path = self._enquiry("delta > resource path", ".camera-snap.wav")
        uri = GLib.filename_to_uri(path)
        self._player.set_uri(uri)
        self._player.play()

    def __init__(self, parent):
        self._parent = parent
        self._player = GstPlayer.Player.new(None, None)
        self._player.set_volume(0.2)
