
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


OPTIONS = [
    (
        "focused-only",
        ord("f"),
        GLib.OptionFlags.NONE,
        GLib.OptionArg.NONE,
        _("focused window only"),
        None
    ),
    (
        "delay",
        ord("d"),
        GLib.OptionFlags.NONE,
        GLib.OptionArg.INT,
        _("delay to take a screenshot in seconds"),
        _("seconds")
    ),
    (
        "version",
        ord("V"),
        GLib.OptionFlags.NONE,
        GLib.OptionArg.NONE,
        _("show version"),
        None
    )
]
