
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import time
from gi.repository import Gdk
from gi.repository import Wnck
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from gi.repository import GstPlayer


class DeltaScreenshot(DeltaEntity):

    def _get_active_window_geometry(self):
        screen = Wnck.Screen.get_default()
        screen.force_update()
        active_window = screen.get_active_window()
        if active_window is None:
            return None
        return active_window.get_geometry()

    def _take_screenshot(self):
        root_window = Gdk.get_default_root_window()
        geometry = None
        if self._enquiry("delta > command line option", "focused-only"):
            geometry = self._get_active_window_geometry()
        if geometry is None:
            geometry = root_window.get_geometry()
        self._pixbuf = Gdk.pixbuf_get_from_window(root_window, *geometry)

    def _play_sound(self):
        self._player = GstPlayer.Player.new(None, None)
        self._player.set_volume(0.2)
        path = self._enquiry("delta > resource path", ".camera-snap.wav")
        uri = GLib.filename_to_uri(path)
        self._player.set_uri(uri)
        self._player.play()

    def _delta_info_pixbuf(self):
        return self._pixbuf

    def __init__(self, parent):
        self._parent = parent
        delay = self._enquiry("delta > command line option", "delay")
        if delay is not None:
            time.sleep(delay)
        self._take_screenshot()
        self._play_sound()
        self._raise("delta > loopback pixbuf ready", self)
