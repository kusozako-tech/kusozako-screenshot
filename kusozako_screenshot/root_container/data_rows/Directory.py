
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3 import HomeDirectory
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from kusozako_screenshot import ModelSignals
from .EntryRow import AlfaEntryRow

POSITION = Gtk.EntryIconPosition.SECONDARY
MODEL = {
    "type": "select-directory",
    "id": "select-directory-to-save-screenshot",
    "title": _("Select Directory"),
    "read-write-only": True
    }


class DeltaDirectory(AlfaEntryRow):

    LABEL = _("Directory :")
    CSS = "secondary-surface-color-class"

    def _set_directory(self, entry):
        text = HomeDirectory.shorten(self._directory)
        entry.set_text(text)
        param = ModelSignals.DIRECTORY_CHANGED, self._directory
        self._raise("delta > model signal", param)

    def _on_icon_press(self, entry, position, button):
        model = MODEL.copy()
        model["directory"] = self._directory
        directory = DeltaFileChooser.run_for_model(self, model)
        if directory is None:
            return
        self._directory = directory
        settings = "screenshot", "default_directory", self._directory
        self._raise("delta > settings", settings)
        self._set_directory(entry)

    def _on_map(self, entry):
        query = "screenshot", "default_directory", GLib.get_home_dir()
        self._directory = self._enquiry("delta > settings", query)
        entry.set_icon_from_icon_name(POSITION, "folder")
        entry.set_icon_tooltip_text(POSITION, _("Select Directory"))
        entry.connect("icon-press", self._on_icon_press)
        entry.set_editable(False)
        self._set_directory(entry)
