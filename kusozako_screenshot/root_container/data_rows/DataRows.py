
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SizeRow import DeltaSizeRow
from .FileName import DeltaFileName
from .Directory import DeltaDirectory


class EchoDataRows:

    def __init__(self, parent):
        DeltaSizeRow(parent)
        DeltaFileName(parent)
        DeltaDirectory(parent)
