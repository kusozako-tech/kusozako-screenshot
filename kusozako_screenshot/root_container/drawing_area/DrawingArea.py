
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from kusozako_screenshot import Size


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _on_draw(self, drawing_area, cairo_context):
        rectangle, _ = drawing_area.get_allocated_size()
        pixbuf = self._enquiry("delta > pixbuf")
        width_ratio = rectangle.width/pixbuf.get_width()
        height_ratio = rectangle.height/pixbuf.get_height()
        ratio = min(1, min(width_ratio, height_ratio))
        preview_pixbuf = pixbuf.scale_simple(
            pixbuf.get_width()*ratio,
            pixbuf.get_height()*ratio,
            GdkPixbuf.InterpType.BILINEAR
            )
        position_x = (rectangle.width-preview_pixbuf.get_width())/2
        Gdk.cairo_set_source_pixbuf(
            cairo_context,
            preview_pixbuf,
            position_x,
            0
            )
        cairo_context.paint()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self, vexpand=True)
        self.connect("draw", self._on_draw)
        self._raise("delta > add to container", self)
