
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_screenshot import ModelSignals


class DeltaApplyButton(Gtk.Button, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, is_valid = user_data
        if signal != ModelSignals.APPLYABLE_CHANGED:
            return
        self.set_sensitive(is_valid)

    def _on_initialize(self):
        self._raise("delta > register model object", self)

    def _on_clicked(self, button):
        param = ModelSignals.APPLY_MODEL, None
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            _("Save"),
            relief=Gtk.ReliefStyle.NONE,
            margin=Unit(0.5)
            )
        self.connect("clicked", self._on_clicked)
        self._on_initialize()
        self._raise("delta > pack end", self)
