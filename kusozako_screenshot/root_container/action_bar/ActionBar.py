
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_screenshot import Size
from .CancelButton import DeltaCancelButton
from .ApplyButton import DeltaApplyButton


class DeltaActionBar(Gtk.Box, DeltaEntity):

    def _delta_call_pack_start(self, button):
        self.pack_start(button, False, False, 0)

    def _delta_call_pack_end(self, button):
        self.pack_end(button, False, False, 0)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            spacing=Size.SPACING
            )
        self.set_margin_top(Size.ROW_MARGIN)
        self.set_size_request(-1, Size.ROW_HEIGHT)
        self._raise("delta > css", (self, "primary-surface-color-class"))
        DeltaCancelButton(self)
        DeltaApplyButton(self)
        self._raise("delta > add to container", self)
