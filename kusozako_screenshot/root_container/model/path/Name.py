
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaName(DeltaEntity):

    SIGNAL = "define acceptable signal here."

    def _action(self, name):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        signal, name = user_data
        if signal != self.SIGNAL:
            return
        self._name = name
        self._action(name)

    def __init__(self, parent):
        self._parent = parent
        self._name = ""
        self._raise("delta > register model object", self)

    @property
    def name(self):
        return self._name
