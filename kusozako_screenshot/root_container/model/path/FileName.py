
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_screenshot import ModelSignals
from .Name import AlfaName


class DeltaFileName(AlfaName):

    SIGNAL = ModelSignals.FILE_NAME_CHANGED

    def _action(self, name):
        is_valid = False
        if name.endswith(".png") and name != ".png":
            is_valid = True
        self._raise("delta > name changed", is_valid)
