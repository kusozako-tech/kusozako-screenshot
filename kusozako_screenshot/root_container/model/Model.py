
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .path.Path import DeltaPath
from .ApplySignal import DeltaApplySignal


class DeltaModel(DeltaEntity):

    def _delta_info_path_is_valid(self):
        return self._path.is_valid

    def _delta_info_path(self):
        return self._path.path

    def __init__(self, parent):
        self._parent = parent
        self._path = DeltaPath(self)
        DeltaApplySignal(self)
