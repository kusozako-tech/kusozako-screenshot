
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.main_loop.MainLoop import AlfaMainLoop
from libkusozako3.main_loop.window.Window import DeltaWindow
from . import APPLICATION_DATA
from .CommandLine import OPTIONS
from .screenshot.Screenshot import DeltaScreenshot
from .root_container.RootContainer import DeltaRootContainer


class DeltaMainLoop(AlfaMainLoop):

    def _delta_call_loopback_main_window_ready(self, parent):
        DeltaRootContainer(parent)

    def _delta_call_loopback_pixbuf_ready(self, parent):
        DeltaWindow(parent)

    def _delta_call_loopback_application_ready(self, parent):
        DeltaScreenshot(parent)

    def _delta_info_command_line_options(self):
        return OPTIONS

    def _delta_info_application_data(self, key):
        return APPLICATION_DATA.get(key, None)

    def _delta_info_application_library_directory(self):
        return GLib.path_get_dirname(__file__)
