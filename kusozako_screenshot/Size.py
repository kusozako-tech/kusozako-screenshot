
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Ux import Unit

SPACING = Unit(1)
ROW_HEIGHT = Unit(6)
ROW_MARGIN = Unit(2)
